import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Box } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';
import { AppStore } from '../AppStore';
import { useStore } from 'laco-react';

const DarkTheme = () => {

    const { isDarkThemeActive } = useStore(AppStore);

    const handleChange = (event) => {
        AppStore.set(
            () => ({ isDarkThemeActive: event.target.checked }),
            'AppStore-onchange'
        );

    }

    return (
        <Box style={{ display: 'flex', justifyContent: 'flex-end', flex: '1' }}>
            <FormControlLabel
                control={
                    <Switch
                        checked={isDarkThemeActive}
                        onChange={handleChange}
                        name="checkedB"
                        color="secondary"
                    />
                }
            />
        </Box>
    )
}
export default DarkTheme;