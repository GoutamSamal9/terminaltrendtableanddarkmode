import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Box } from '@material-ui/core';
import { AppStore } from '../AppStore';
import { useStore } from 'laco-react';



const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    textColor: {
        color: theme.palette.common.white
    },
    tableHeader: {
        fontWeight: 600,
        fontSize: 26,
        textAlign: 'center',
        padding: theme.spacing(2, 0),
    },
    tableHeaderWite: {
        fontWeight: 600,
        fontSize: 26,
        textAlign: 'center',
        padding: theme.spacing(2, 0),
        color: theme.palette.common.white
    },
    tableSmallText: {
        fontWeight: 600,
        fontSize: 20,
        textAlign: 'center',
        padding: theme.spacing(1, 0)
    },
    tableHeading: {
        fontWeight: 600
    },
    bigTestWhite: {
        color: theme.palette.common.white,
        fontWeight: 600
    },
    smallTextWhite: {
        color: theme.palette.common.white
    }
}))



export default function TableCom({ tableName, tableData, tableHeading, tableSmallText }) {
    const classes = useStyles();

    const { isDarkThemeActive } = useStore(AppStore);
    return (
        <TableContainer>
            {tableName ? <Box className={isDarkThemeActive ? classes.tableHeaderWite : classes.tableHeader}>{tableName}</Box> : null}
            {tableSmallText ? <Box className={classes.tableSmallText}>{tableSmallText}</Box> : null}
            <Table className={classes.table} aria-label="simple table">
                <TableHead>

                    <TableRow > {
                        tableHeading.map((each, i) => (
                            <TableCell key={i} className={isDarkThemeActive ? classes.bigTestWhite : classes.tableHeading} align="center">{each.name}</TableCell>))
                    }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        tableData.map((each, i) => (
                            <TableRow key={i}>
                                <TableCell align="center" component="th" scope="row" className={isDarkThemeActive ? classes.smallTextWhite : ''}>
                                    {each.name}
                                </TableCell>
                                <TableCell align="center" component="th" scope="row" className={isDarkThemeActive ? classes.smallTextWhite : ''}>
                                    {each.time}
                                </TableCell>
                                <TableCell align="center" component="th" scope="row" className={isDarkThemeActive ? classes.smallTextWhite : ''}>
                                    {each.location}
                                </TableCell>

                            </TableRow>
                        ))
                    }

                </TableBody>
            </Table>
        </TableContainer>
    );
}
