import React from 'react';
import tableData from '../TableData.json';
import TableCom from './TableCom';


let tableHeading = [
    {
        name: "Name"
    },
    {
        name: "Time"
    },
    {
        name: "Location"
    },
]


export default function Labs() {


    return (
        <>
            <TableCom tableHeading={tableHeading} tableData={tableData.labs} tableName="Labs" />
        </>

    );
}
