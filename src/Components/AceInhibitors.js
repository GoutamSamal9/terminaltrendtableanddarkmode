import React from 'react';
import tableData from '../TableData.json';
import TableTwo from './TableTwo';
import tableHeading from '../TableHeading';
import { Box } from '@material-ui/core';

export default function AceInhibitors() {

    return (
        <Box mt={2}>
            <TableTwo tableHeading={tableHeading} tableData={tableData.medications[0].aceInhibitors} tableSmallText="AceInhibitors" />
        </Box>

    );
}
