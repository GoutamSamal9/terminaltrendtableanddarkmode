import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { Box } from '@material-ui/core';
import { AppStore } from '../AppStore';
import { useStore } from 'laco-react';



const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },
    tableHeader: {
        fontWeight: 600,
        fontSize: 26,
        textAlign: 'center',
        padding: theme.spacing(2, 0)
    },

    tableHeading: {
        fontWeight: 600
    },
    bigTestWhite: {
        color: theme.palette.common.white,
        fontWeight: 600
    },
    tableSmallText: {
        fontWeight: 600,
        fontSize: 20,
        textAlign: 'left',
        padding: theme.spacing(1, 2)
    },
    tableSmallTextWhite: {
        fontWeight: 600,
        fontSize: 20,
        textAlign: 'left',
        padding: theme.spacing(1, 2),
        color: theme.palette.common.white
    },
    smallTextWhite: {
        color: theme.palette.common.white
    }
}))



export default function TableTwo({ tableName, tableData, tableHeading, tableSmallText }) {
    const classes = useStyles();
    const { isDarkThemeActive } = useStore(AppStore);

    return (
        <TableContainer >
            {tableName ? <Box className={classes.tableHeader}>{tableName}</Box> : null}
            {tableSmallText ? <Box className={isDarkThemeActive ? classes.tableSmallTextWhite : classes.tableSmallText}>{tableSmallText}</Box> : null}
            <Table className={classes.table} aria-label="simple table">
                <TableHead>

                    <TableRow > {
                        tableHeading.map((each, i) => (
                            <TableCell key={i} className={isDarkThemeActive ? classes.bigTestWhite : classes.tableHeading} align="center">{each.name}</TableCell>))
                    }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        tableData.map((each, i) => (
                            <TableRow key={i}>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''}  >
                                    {each.name}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.strength}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.dose}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.route}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.sig}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.pillCount}
                                </TableCell>
                                <TableCell align="center" className={isDarkThemeActive ? classes.smallTextWhite : ''} >
                                    {each.refills}
                                </TableCell>
                            </TableRow>
                        ))
                    }

                </TableBody>
            </Table>
        </TableContainer>
    );
}
