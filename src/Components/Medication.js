import React from 'react';
import { Box, makeStyles } from '@material-ui/core';
import AceInhibitors from './AceInhibitors';
import Antianginal from './Antianginal';
import Anticoagulants from './Anticoagulants';
import BetaBlocker from './BetaBlocker';
import Diuretic from './Diuretic';
import Mineral from './Mineral';
import { AppStore } from '../AppStore';
import { useStore } from 'laco-react';


const useStyles = makeStyles((theme) => ({
    tableHeaderWite: {
        fontWeight: 600,
        fontSize: 26,
        textAlign: 'center',
        padding: theme.spacing(2, 0),
        color: theme.palette.common.white
    },

    tableHeader: {
        fontWeight: 600,
        fontSize: 26,
        textAlign: 'center',
        padding: theme.spacing(2, 0)
    }
}))

function Medication() {

    const classes = useStyles();
    const { isDarkThemeActive } = useStore(AppStore);

    return (
        <Box>
            <Box className={isDarkThemeActive ? classes.tableHeaderWite : classes.tableHeader}>Medication</Box>
            <AceInhibitors />
            <Antianginal />
            <Anticoagulants />
            <BetaBlocker />
            <Diuretic />
            <Mineral />
        </Box>
    );
}

export default Medication;
