import { Store } from 'laco';

export const AppStore = new Store({
    isDarkThemeActive: false
}, 'AppStore')
