
let tableHeading = [
    {
        name: "Name"
    },
    {
        name: "Strength"
    },
    {
        name: "Dose"
    },
    {
        name: "Route"
    },
    {
        name: "Sig"
    },
    {
        name: "PillCount"
    },
    {
        name: "Refills"
    },
]
export default tableHeading;