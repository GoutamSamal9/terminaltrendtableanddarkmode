import React from 'react';
import { Box, makeStyles } from '@material-ui/core';
import Medication from './Components/Medication';
import Labs from './Components/Labs';
import Imaging from './Components/Imaging';
import { useStore } from 'laco-react';
import { AppStore } from './AppStore';
import DarkTheme from './Components/DarkTheme';

const useStyles = makeStyles((theme) => ({
  darkBackground: {
    background: theme.palette.common.black
  },
  whiteBackground: {
    background: theme.palette.common.white
  }
}))


function App() {

  const classes = useStyles();

  const { isDarkThemeActive } = useStore(AppStore);


  return (
    <Box className={!isDarkThemeActive ? classes.whiteBackground : classes.darkBackground}>

      <DarkTheme />

      <Medication />
      <Labs />
      <Imaging />
    </Box>
  );
}

export default App;
